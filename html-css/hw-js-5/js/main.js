"use strict";
function createNewUser() {
	const userFirstName = prompt("what your Name?");
	const userLastName = prompt('what your Last Name?');
	const userBirthday = prompt('Date of birth');

	if (userFirstName === null || userLastName === null || userBirthday === null) {
		return;
	}

	if (!userFirstName.trim() || !userLastName.trim()) {
		console.log("не коректно ввел данные ");
		return createNewUser();
	}

	const userBirthdayValue = userBirthday.split('.');
	if(userBirthdayValue.length !== 3 || isNaN(userBirthdayValue[0]) || isNaN(userBirthdayValue[1]) || isNaN(userBirthdayValue[2])){
		return createNewUser();
	}


   const userBirthdayDate = new Date(userBirthdayValue[2], userBirthdayValue[1] - 1, userBirthdayValue[0]);

	let newUser = {
		firstName: userFirstName,
		lastName: userLastName,
		birthday: userBirthdayDate,

		getPassword: function() {
			let d = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
			return d;
		},
		getAge: function () {
			let ageUser = ( Date.now() - this.birthday ) / ( 1000 * 60 * 60 * 24 * 365.2425); 
			return Math.trunc(ageUser);
		}
	}
	return newUser;
};

let user = createNewUser();
if(user){
	let age = user.getAge();
	let nameyear  = user.getPassword();
	console.log(`age ${age},`, nameyear);
}


