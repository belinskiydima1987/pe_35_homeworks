"use stri";

const tabsBtn = document.querySelectorAll('.tabs-title__item');

tabsBtn.forEach(function (item) {
	item.addEventListener('click',function() {
		let activeLink = document.querySelectorAll('.tabs .active');
		if(activeLink){
			activeLink[0].classList.remove('active');
		}
		
		
		let activeTab = document.querySelectorAll('.tabs-content .active');
		if(activeTab){
			activeTab[0].classList.remove('active');
		}
		let tab = document.getElementById(item.dataset.tabeName);
		item.classList.add('active');
		tab.classList.add('active');
		
	});
});

