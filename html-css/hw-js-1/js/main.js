"use strict";

function askName() {
	let userName = prompt('What is your name ?');
	if (userName == null) {
		return null;
	}

	if (!userName || !isNaN(userName) || !userName.trim()) {
		alert('please enter valid name');
		askName();
	}
	return userName;
}

function welcome(enteredName) {
	alert('welcome, ' +  enteredName);
}

function notAllowed() {
	alert('You are not allowed to visit this website');
}

function askAge(enteredName) {
	let age = prompt('how old are you ?');

	if (age == null) {
		return;
	}

	if (!age || isNaN(age)) {
		alert('please enter valid age');
		askAge(enteredName);
		return;
	}

	if (age < 18) {
		notAllowed();
		return;
	} else if (age <= 18 || age <= 22) {
		let c = confirm('Are you sure you want to continue?');
		if (c) {
			welcome(enteredName);
			return;
		} else {
			notAllowed();
			return;
		}

	} if (age > 22) {
		welcome(enteredName);
		return;
	}

}

let name = askName();
if (name !== null) {
	askAge(name);
}