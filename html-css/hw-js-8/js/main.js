"use strict";

let container = document.getElementById("message");
let price = document.getElementById("price");
let priceForm = document.getElementById('price-form');

function clearContainer() {
	container.innerHTML = "";
}

price.addEventListener('blur', e => {

	console.log(e);

	clearContainer();
	let clearButton = document.createElement("button");
	let errorMessage = document.getElementById("error");
	if(errorMessage){
		errorMessage.remove();
	}
	
	if (+e.target.value > 0){
		let priceValue = +e.target.value;

		let priceMessage = document.createElement("span");
		priceMessage.innerHTML = `Текущая цена: ${priceValue}$`;
		
		clearButton.innerText = "x";
		clearButton.className = "btn";
		clearButton.addEventListener('click', e => {
			clearContainer();
		});
		container.prepend(clearButton);
		container.prepend(priceMessage);
	
	} else {
		errorMessage = document.createElement('span');
		errorMessage.id = "error";
		errorMessage.innerHTML = "Please enter correct price!";

		priceForm.append(errorMessage);
		clearContainer();
	
	}

	e.target.value = '';

});

