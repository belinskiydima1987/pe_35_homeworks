"use strict";


let playIntervale;
imegesInterval();
function imegesInterval() {
	let activeImg = document.getElementsByClassName('active')[0];
	let nextImg = activeImg.nextElementSibling;
		if(!nextImg){
			nextImg = activeImg.parentElement.firstElementChild;
		}
		activeImg.classList.remove('active');
		nextImg.classList.add('active');
		playIntervale = setTimeout(imegesInterval, 1000);

};

let stopButton = document.getElementById('stop').onclick = function () {
	clearTimeout(playIntervale);
};
let playButton = document.getElementById('play').onclick = function () {
	imegesInterval();
};
