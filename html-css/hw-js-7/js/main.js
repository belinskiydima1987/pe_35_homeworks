"use strict";
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).
// Технические требования:
// Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// Каждый из элементов массива вывести на страницу в виде пункта списка;
// Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его на страницу;
// Примеры массивов, которые можно выводить на экран:
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];


let block = document.getElementById('conteiner1')
function creatList(array, parent) {
	let newArray = [];
	let list = array.map(function (element) {
		return `<li>${element}</li>`;
	});
	let ul = document.createElement('ul');
	ul.innerHTML = list.join('');
	if (!parent){
		parent = document.body;
	}
	parent.append(ul);	
}
creatList( ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", "list"], block);