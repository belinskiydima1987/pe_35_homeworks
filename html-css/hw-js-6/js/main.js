"use strict";
// =====дз6
// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент
// - массив, который будет содержать в себе любые данные, второй аргумент 
// - тип данных.
// Функция должна вернуть новый массив, который будет содержать в себе все данные,
// которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. 
// То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string',
// то функция вернет массив [23, null].

function filterBy(array, type) {
	let newArray = [];

	for (let i = 0; i < array.length; i++) {
		let element = array[i];
		if(typeof element !== type){
			newArray.push(element);
		}
	}
	console.log(array);
	return newArray;
}
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'))




