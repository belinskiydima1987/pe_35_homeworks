"use strict";
document.onkeypress = function (event) {
	let buttons = document.getElementsByClassName("btn");
	for (let item of buttons) {
		if(item.textContent.toLowerCase() === event.key.toLowerCase()){
			item.classList.add('blue');
		} else {
			item.classList.remove("blue");
		}
	}
}
