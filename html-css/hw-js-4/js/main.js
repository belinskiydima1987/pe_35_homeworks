"use strict";

//домашка 5 object
// Задание
// Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript,
//  без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.

// Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.

// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, 
// соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
//  Вывести в консоль результат выполнения функции.

// Необязательное задание продвинутой сложности:

// Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. 
// Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

function createNewUser(){

	let userFirstName = prompt("what your Name?")
	let userLastName = prompt('what your Last Name?')
	if (userFirstName === null || userLastName === null) {
		return;
	}
	if (!userFirstName || !userLastName || !userFirstName.trim() || !userLastName.trim()) {
		console.log("не коректно ввел данные ");
		return createNewUser();
	}
	let newUser = {
		firstName: userFirstName,
		lastName: userLastName,
		getLogin: function () {
			return this.firstName[0].toLowerCase() + "." + this.lastName.toLowerCase();
		}
	}
	return newUser;
	
}
let user = createNewUser();
if(user){
	let login = user.getLogin();
	console.log(login)
}

