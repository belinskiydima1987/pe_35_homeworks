

let theme1 = "blue-theme";
let theme2 = "red-theme";
let bodyTag = document.body;

function changeTheme(newTheme, currentTheme) {
	if(currentTheme){
		bodyTag.classList.remove(currentTheme);
	}

	bodyTag.classList.add(newTheme);
	localStorage.setItem('theme', newTheme);
}

window.onload = function(){
	let theme = localStorage.getItem('theme') || theme1;
	changeTheme(theme);
};

document.getElementById('changeTheme').onclick = function () {
	let currentTheme, newTheme;
	if (bodyTag.classList.contains(theme1)){
		currentTheme = theme1;
		newTheme = theme2;
	} else {
		currentTheme = theme2;
		newTheme = theme1;
	}
	changeTheme(newTheme, currentTheme)
};
